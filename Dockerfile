FROM --platform=linux/amd64 ubuntu:lunar

ARG DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash", "-c"]

RUN apt-get update
RUN apt-get install -yqq --no-install-recommends \
  binutils \
  binutils-dev \
  build-essential \
  cmake \
  git \
  g++ \
  gcc \
  gnupg \
  ninja-build \
  libopenmpi-dev \
  openssh-client \
  patch \
  python-is-python3 \
  python3 \
  python3-pip \
  python3-venv \
  sudo \
  unzip \
  vim \
  wget



COPY docker/install-llvm.sh /scripts/
RUN /scripts/install-llvm.sh lunar 15

COPY docker/install-sonar.sh /scripts/
RUN /scripts/install-sonar.sh 4.8.0.2856 45a9a54dfe5f58b554e9b40ad3becbf9871a4eddb1c2892b67cf191cdd891754

RUN apt-get autoremove -yqq

ARG PARCOACH_VERSION=2.4.0
RUN wget https://gitlab.inria.fr/api/v4/projects/12320/packages/generic/parcoach/${PARCOACH_VERSION}/parcoach-${PARCOACH_VERSION}-shared-Linux.tar.gz -O /scripts/parcoach.tgz
RUN tar -C /usr --strip-components=1 -xzf /scripts/parcoach.tgz

# Don't do this at home
RUN echo 'ubuntu ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER ubuntu
ENV SHELL=/bin/bash
WORKDIR /home/ubuntu
